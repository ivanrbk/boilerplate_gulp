const gulp = require('gulp');
const sass = require('gulp-sass');
const plumber = require('gulp-plumber');
const normalize = require('node-normalize-scss');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const csso = require('gulp-csso');
const rename = require('gulp-rename');
const imagemin = require('gulp-imagemin');
const del = require('del');
const server = require('browser-sync').create();
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const hash = require('gulp-hash-filename');
const htmlreplace = require('gulp-html-replace');

let hashedCSS, hashedJS, hashedVendorJS;

const path = {
  src: {
    htmlAll: 'source/*.html',
    sass: 'source/sass/style.scss',
    js: 'source/js/main.js',
    jsVendor: ['source/js/vendor/classList.min.js',
              'source/js/vendor/moment.min.js',
              'source/js/vendor/moment-locale-ru.js',
              'source/js/vendor/flatpickr.min.js',
              'source/js/vendor/highcharts.js',
              'source/js/vendor/ru.js'],
    img: 'source/img/**/*.{png,jpg,svg}',
    copy: [
      'source/fonts/**/*.{ttf,woff,woff2}',
      'source/img/**',
      'source/css/**',
      'source/video/**',
      '!source/js/vendor/**',
      'source/*.ico',
      'source/*.png'
    ]
  },
  build: {
    root: 'build',
    css: 'build/css',
    js: 'build/js',
  }
};

gulp.task('css:dev', function () {
  return gulp.src(path.src.sass)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass({
      includePaths: normalize.includePaths
    }))
    .pipe(sourcemaps.write())
    .pipe(postcss([
      autoprefixer()
    ]))
    .pipe(gulp.dest(path.build.css))
    .pipe(server.stream());
});

gulp.task('css:prod', function () {
  return gulp.src(path.src.sass)
    .pipe(plumber())
    .pipe(sass({
      includePaths: normalize.includePaths
    }))
    .pipe(postcss([
      autoprefixer()
    ]))
    .pipe(gulp.dest(path.build.css))
    .pipe(csso())
    .pipe(hash())
    .pipe(rename(function (path) {
      path.basename += '-min';
      hashedCSS = path.basename + '.css';
      console.log('hashedCSS = ' + hashedCSS);
    }))
    .pipe(gulp.dest(path.build.css))
    .pipe(server.stream());
});

gulp.task('js:dev', function () {
  return gulp.src(path.src.js)
    .pipe(babel({
			presets: ['@babel/preset-env']
      }))
    .pipe(concat('main.js', {newLine: ';'}))
    .pipe(gulp.dest(path.build.js));
});

gulp.task('js:prod', function () {
  return gulp.src(path.src.js)
    .pipe(babel({
			presets: ['@babel/preset-env']
      }))
    .pipe(uglify())
    .pipe(concat('main.js', {newLine: ';'}))
    .pipe(hash())
    .pipe(rename(function (path) {
      path.basename += '-min';
      hashedJS = path.basename + '.js';
			console.log('hashedJS = ', hashedJS)
    }))
    .pipe(gulp.dest(path.build.js));
});

gulp.task('js:vendor', function () {
  return gulp.src(path.src.jsVendor)
    .pipe(concat('vendor.js', {newLine: ';'}))
    .pipe(gulp.dest(path.build.js))
    .pipe(hash())
    .pipe(rename(function (path) {
      path.basename += '-min';
      hashedVendorJS = path.basename + '.js';
			console.log('hashedVendorJS = ', hashedVendorJS)
    }))
    .pipe(gulp.dest(path.build.js));
});

gulp.task('html:dev', function () {
  return gulp.src(path.src.htmlAll)
    .pipe(htmlreplace({
      'css': path.DEV_PATH + '/css/style.css',
      'js': [path.DEV_PATH + '/js/vendor.js', path.DEV_PATH + '/js/main.js']
    }))
    .pipe(gulp.dest(path.build.root));
});

gulp.task('html:prod', function () {
  return gulp.src(path.src.htmlAll)
    .pipe(htmlreplace({
      'css': path.PROD_PATH + '/css/' + hashedCSS,
      'js': [path.PROD_PATH + '/js/' + hashedVendorJS, path.PROD_PATH + '/js/' + hashedJS]
    }))
    .pipe(gulp.dest(path.build.root));
});

gulp.task('images', function () {
  return gulp.src(path.src.img)
    .pipe(imagemin([
      imagemin.optipng({optimizationLevel: 3}),
      imagemin.jpegtran({progressive: true}),
      imagemin.svgo()
    ]))
    .pipe(gulp.dest(path.src.img));
});

gulp.task('copy', function () {
  return gulp.src(path.src.copy, {
    base: 'source'
  })
  .pipe(gulp.dest(path.build.root));
});

gulp.task('clean', function () {
  return del(path.build.root);
});

gulp.task('refresh', function (done) {
	server.reload();
	done();
});

gulp.task('server', function () {
  server.init({
    server: path.build.root + '/',
    notify: false,
    open: true,
    cors: true,
    ui: false
  });

  gulp.watch('source/sass/**/*.{scss,sass}', gulp.series('css:dev','html:dev','refresh'));
  gulp.watch('source/js/*.js', gulp.series('js:dev', 'html:dev', 'refresh'));
  gulp.watch('source/*.html', gulp.series('html:dev', 'refresh'));
});

gulp.task('build-dev', gulp.series(
'clean',
'copy',
'css:dev',
'js:vendor',
'js:dev',
'html:dev'
));

gulp.task('build-prod', gulp.series(
  'clean',
  'copy',
  'css:prod',
  'js:vendor',
  'js:prod',
  'html:prod'
));

gulp.task('start', gulp.series('build-dev', 'server'));
